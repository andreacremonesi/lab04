# Riferimenti

- [Setup di Eiffel Studio in ambiente Linux](https://www.eiffel.org/doc/eiffelstudio/Linux)
- [Manuale compilatore](https://www.eiffel.org/doc/eiffelstudio/Command%20line)
- [Standard ECMA-367 "Eiffel: Analysis, Design and Programming Language"](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-367.pdf) Si
  tratta di un documento molto leggibile per... gli standard degli standard: il
  capitolo 7 è un'ottima introduzione a Eiffel e la progettazione
  *Object-Oriented*
- [Differenze fra ECMA-367 e Eiffel Studio](https://www.eiffel.org/doc/eiffelstudio/Differences%20between%20standard%20ECMA-367%20and%20Eiffel%20Software%20implementation)


# Esercizio 1

(tempo stimato 45')

Scrivere un nuovo *plugin* che permetta di compilare (*melt*) e finalizzare
(*finish_freezing*) un programma Eiffel (sottoprogetto `eiffel`, per includerlo
aggiungere `include 'eiffel'` in `settings.gradle`). Si noti che:

- Tutti gli strumenti Eiffel Studio necessitano della corretta impostazione
  delle variabili d'ambiente: `ISE_EIFFEL` (p.es. `/opt/Eiffel_16.05`) e
  `ISE_PLATFORM` (p.es. `linux-x86-64`).
- Il compilatore `ec` e il finalizzatore `finish_freezing` si trovano nella
  *directory* `$ISE_EIFFEL/studio/spec/$ISE_PLATFORM/bin`
- Le opzioni dei *tool* possono essere elencate con l'opzione `-help`
- La finalizzazione dipende dalla corretta compilazione
- Può essere opportuno fare creare tutti i *file* di appoggio in una opportuna
  *directory* di *build*, in modo che sia facile avere anche un *task* di
  cancellazione (`clean`).
- La documentazione sulla scrittura di *plugin*
  è [qui](https://docs.gradle.org/current/userguide/custom_plugins.html) e un
  esempio è contenuto nella *directory* `buildSrc/src/main/groovy`. Il *plugin*
  d'esempio è attivato aggiungendo nel `build.gradle` nella *directory*
  `eiffel/` (con `apply plugin: GreetingPlugin`), e può essere provato con il
  comando `gradle hello` (eventualmente configurando `greeting.message`).
  
# Esercizio 2

(tempo stimato 45')

Reimplementare in Eiffel il Bowling, partendo dai test di Uncle Bob, per comodità riportati anche qui:

```java
public class BowlingGameTest extends TestCase {
    private Game g;

    protected void setUp() throws Exception {
        g = new Game();
    }

    @Test
    public void testGutterGame() throws Exception {
        rollMany(20, 0);
        assertEquals(0, g.score());
    }

    @Test
    public void testAllOnes() throws Exception {
        rollMany(20,1);
        assertEquals(20, g.score());
    }

    @Test
    public void testOneSpare() throws Exception {
        rollSpare();
        g.roll(3);
        rollMany(17,0);
        assertEquals(16,g.score());
    }

    @Test
    public void testOneStrike() throws Exception {
        rollStrike();
        g.roll(3);
        g.roll(4);
        rollMany(16, 0);
        assertEquals(24, g.score());
    }

    @Test
    public void testPerfectGame() throws Exception {
        rollMany(12,10);
        assertEquals(300, g.score());
    }

    @Test
    public void testLastSpare() throws Exception {
        rollMany(9,10);
        rollSpare();
        g.roll(10);
        assertEquals(275, g.score());
    }


    private void rollSpare() {
        g.rollMany(2, 5);
    }

    private void rollStrike() {
        g.roll(10);
    }

    private void rollMany(int n, int pins) {
        for (int i = 0; i < n; i++)
            g.roll(pins);
    }
}
```

Per i test, usare il *framework* `EQA_TEST_SET` (analogo a `junit`)

```eiffel
class
	GAME_TEST_SET

inherit
    
    EQA_TEST_SET

-- ....
```


# Esercizio 3

(tempo stimato 45')

Trasformare i test di Uncle Bob in opportuni contratti per le *feature*  della classe `Game`

```eiffel
	roll (pins: INTEGER)

	score: INTEGER
```
